/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp.entity;



/**
 *
 * @author 3D-Artist
 */
public class Agence {
    private int id_agence;
    private String nom_agence;
    private String rue;
    private int code_postal;
    private String ville;
    private int telephone_agence;
    private String type_agence;
    private String horaire_travail;
    private String photo_agence;
    private User owner;
    private String piece_justificatif;
    private double latitude;
    private double longitude;
    private boolean approuved;

    
    public Agence() {
    }

    public Agence(int id_agence, String nom_agence, String rue, int code_postal, String ville, int telephone_agence, String type_agence, String horaire_travail, String photo_agence, User owner, String piece_justificatif, double latitude, double longitude, boolean approuved) {
        this.id_agence = id_agence;
        this.nom_agence = nom_agence;
        this.rue = rue;
        this.code_postal = code_postal;
        this.ville = ville;
        this.telephone_agence = telephone_agence;
        this.type_agence = type_agence;
        this.horaire_travail = horaire_travail;
        this.photo_agence = photo_agence;
        this.owner = owner;
        this.piece_justificatif = piece_justificatif;
        this.latitude = latitude;
        this.longitude = longitude;
        this.approuved = approuved;
    }

    public Agence(String nom_agence, String rue, int code_postal, String ville, int telephone_agence, String type_agence, String horaire_travail, String photo_agence, User owner, String piece_justificatif, double latitude, double longitude, boolean approuved) {
        this.nom_agence = nom_agence;
        this.rue = rue;
        this.code_postal = code_postal;
        this.ville = ville;
        this.telephone_agence = telephone_agence;
        this.type_agence = type_agence;
        this.horaire_travail = horaire_travail;
        this.photo_agence = photo_agence;
        this.owner = owner;
        this.piece_justificatif = piece_justificatif;
        this.latitude = latitude;
        this.longitude = longitude;
        this.approuved = approuved;
    }

    public int getId_agence() {
        return id_agence;
    }

    public void setId_agence(int id_agence) {
        this.id_agence = id_agence;
    }

    public String getNom_agence() {
        return nom_agence;
    }

    public void setNom_agence(String nom_agence) {
        this.nom_agence = nom_agence;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public int getCode_postal() {
        return code_postal;
    }

    public void setCode_postal(int code_postal) {
        this.code_postal = code_postal;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public int getTelephone_agence() {
        return telephone_agence;
    }

    public void setTelephone_agence(int telephone_agence) {
        this.telephone_agence = telephone_agence;
    }

    public String getType_agence() {
        return type_agence;
    }

    public void setType_agence(String type_agence) {
        this.type_agence = type_agence;
    }

    public String getHoraire_travail() {
        return horaire_travail;
    }

    public void setHoraire_travail(String horaire_travail) {
        this.horaire_travail = horaire_travail;
    }

    public String getPhoto_agence() {
        return photo_agence;
    }

    public void setPhoto_agence(String photo_agence) {
        this.photo_agence = photo_agence;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public String getPiece_justificatif() {
        return piece_justificatif;
    }

    public void setPiece_justificatif(String piece_justificatif) {
        this.piece_justificatif = piece_justificatif;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public boolean isApprouved() {
        return approuved;
    }

    public void setApprouved(boolean approuved) {
        this.approuved = approuved;
    }

    @Override
    public String toString() {
        return nom_agence;
    }
    
}
