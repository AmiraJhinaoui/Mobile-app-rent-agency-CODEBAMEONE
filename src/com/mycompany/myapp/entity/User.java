/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp.entity;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author 3D-Artist
 */
public class User {
    
    private int id;
    private String nom;
    private String prenom;
    private String password;
    private Date date_naissance;
    private int telephone;
    private String mail;
    private String adresse;
    private boolean approuved;
    private boolean banned;
    private String role;
    private String image;
    private List<Agence> listeAgences;

    public User(int id, String nom, String prenom, String password, Date date_naissance, int telephone, String mail, String adresse, boolean approuved, boolean banned, String role, String image) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.password = password;
        this.date_naissance = date_naissance;
        this.telephone = telephone;
        this.mail = mail;
        this.adresse = adresse;
        this.approuved = approuved;
        this.banned = banned;
        this.role = role;
        this.image = image;
        this.listeAgences = new ArrayList<>();
    }

    

    public User() {
        this.listeAgences = new ArrayList<>();
    }
    
    public void addAgence(Agence agence)
    {
        this.listeAgences.add(agence);
    }
    
    public int getSizeListeAgences()
    {
        return this.listeAgences.size();
    }
    
    

    public void setId(int id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setDate_naissance(Date date_naissance) {
        this.date_naissance = date_naissance;
    }

    public void setTelephone(int telephone) {
        this.telephone = telephone;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public void setApprouved(boolean approuved) {
        this.approuved = approuved;
    }

    public void setBanned(boolean banned) {
        this.banned = banned;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getPassword() {
        return password;
    }

    public Date getDate_naissance() {
        return date_naissance;
    }

    public int getTelephone() {
        return telephone;
    }

    public String getMail() {
        return mail;
    }

    public String getAdresse() {
        return adresse;
    }

    public boolean isApprouved() {
        return approuved;
    }

    public boolean isBanned() {
        return banned;
    }

    public String getRole() {
        return role;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<Agence> getListeAgences() {
        return listeAgences;
    }

    public void setListeAgences(List<Agence> listeAgences) {
        this.listeAgences = listeAgences;
    }
    
    

    @Override
    public String toString() {
        return nom + " " + prenom ;
    }
    
    

    
    
    
    
    
}
