/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp.entity;



/**
 *
 * @author Amira
 */
public class Voiture {
     private String matricule;
    private String marque;
    private String couleur;
    private String carburant;
    private java.util.Date age;
    private double kilometrage;
    private int puissance;
    private boolean disponible;
    private User owner;
    private String carrousserie;
    private String boite;
    private boolean gps;
    private boolean climatisation;
    private boolean airbag;
    private int nbr_porte;
    private boolean frein_abs;
    private boolean alarme;
    private String description;
    private Agence agence;
    private float prixLocation;
    
    public Voiture() {
    }

    public Voiture(String matricule, String marque, String couleur, String carburant, java.util.Date age, double kilometrage, int puissance, boolean disponible, User owner, String carrousserie, String boite, boolean gps, boolean climatisation, boolean airbag, int nbr_porte, boolean frein_abs, boolean alarme, String description, Agence agence, float prixLocation) {
        this.matricule = matricule;
        this.marque = marque;
        this.couleur = couleur;
        this.carburant = carburant;
        this.age = age;
        this.kilometrage = kilometrage;
        this.puissance = puissance;
        this.disponible = disponible;
        this.owner = owner;
        this.carrousserie = carrousserie;
        this.boite = boite;
        this.gps = gps;
        this.climatisation = climatisation;
        this.airbag = airbag;
        this.nbr_porte = nbr_porte;
        this.frein_abs = frein_abs;
        this.alarme = alarme;
        this.description = description;
        this.agence = agence;
        this.prixLocation = prixLocation;
    }

    
    
    
    
    


    public boolean isDisponible() {
        return disponible;
    }

    public Agence getAgence() {
        return agence;
    }

    public boolean isGps() {
        return gps;
    }

    public boolean isClimatisation() {
        return climatisation;
    }

    public boolean isAirbag() {
        return airbag;
    }

    public boolean isFrein_abs() {
        return frein_abs;
    }

    public boolean isAlarme() {
        return alarme;
    }

    public void setAgence(Agence agence) {
        this.agence = agence;
    }
    

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public void setCarburant(String carburant) {
        this.carburant = carburant;
    }

    public void setAge(java.util.Date age) {
        this.age = age;
    }

    public void setKilometrage(double kilometrage) {
        this.kilometrage = kilometrage;
    }

    public void setPuissance(int puissance) {
        this.puissance = puissance;
    }

    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }

    public void setCarrousserie(String carrousserie) {
        this.carrousserie = carrousserie;
    }

    public void setBoite(String boite) {
        this.boite = boite;
    }

    public void setGps(boolean gps) {
        this.gps = gps;
    }

    public void setClimatisation(boolean climatisation) {
        this.climatisation = climatisation;
    }

    public void setAirbag(boolean airbag) {
        this.airbag = airbag;
    }

    public void setNbr_porte(int nbr_porte) {
        this.nbr_porte = nbr_porte;
    }

    public void setFrein_abs(boolean frein_abs) {
        this.frein_abs = frein_abs;
    }

    public void setAlarme(boolean alarme) {
        this.alarme = alarme;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    

    public String getMatricule() {
        return matricule;
    }

    public String getMarque() {
        return marque;
    }

    public String getCouleur() {
        return couleur;
    }

    public String getCarburant() {
        return carburant;
    }

    public java.util.Date getAge() {
        return age;
    }

    public double getKilometrage() {
        return kilometrage;
    }

    public int getPuissance() {
        return puissance;
    }

    public boolean getDisponible() {
        return disponible;
    }

    public String getCarrousserie() {
        return carrousserie;
    }

    public String getBoite() {
        return boite;
    }

    public boolean getGps() {
        return gps;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public User getOwner() {
        return owner;
    }

    public boolean getClimatisation() {
        return climatisation;
    }

    public boolean getAirbag() {
        return airbag;
    }

    public int getNbr_porte() {
        return nbr_porte;
    }

    public boolean getFrein_abs() {
        return frein_abs;
    }

    public boolean getAlarme() {
        return alarme;
    }

    public String getDescription() {
        return description;
    }

    public float getPrixLocation() {
        return prixLocation;
    }

    public void setPrixLocation(float prixLocation) {
        this.prixLocation = prixLocation;
    }

    @Override
    public String toString() {
        return matricule;
    }

    //public void setNom(String newValue) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
   //}

    
}
