/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp.entity;

import java.util.Date;




public class Location {
    
    private User client;
    private Agence agence;
    private Voiture voiture;
    private Date dateDebut;
    private Date dateFin;
    private float prixLocation;
    private boolean avecChauffeur;
    private boolean approuved;

    public Location() {
    }

    public Location(User client, Agence agence, Voiture voiture, Date dateDebut, Date dateFin, float prixLocation, boolean avecChauffeur, boolean approuved) {
        this.client = client;
        this.agence = agence;
        this.voiture = voiture;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.prixLocation = prixLocation;
        this.avecChauffeur = avecChauffeur;
        this.approuved = approuved;
    }

    public User getClient() {
        return client;
    }

    public void setClient(User client) {
        this.client = client;
    }

    public Agence getAgence() {
        return agence;
    }

    public void setAgence(Agence agence) {
        this.agence = agence;
    }

    public Voiture getVoiture() {
        return voiture;
    }

    public void setVoiture(Voiture voiture) {
        this.voiture = voiture;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public float getPrixLocation() {
        return prixLocation;
    }

    public void setPrixLocation(float prixLocation) {
        this.prixLocation = prixLocation;
    }

    public boolean isAvecChauffeur() {
        return avecChauffeur;
    }

    public void setAvecChauffeur(boolean avecChauffeur) {
        this.avecChauffeur = avecChauffeur;
    }

    public boolean isApprouved() {
        return approuved;
    }

    public void setApprouved(boolean approuved) {
        this.approuved = approuved;
    }

    @Override
    public String toString() {
        return "Location{" + "client=" + client + ", agence=" + agence + ", voiture=" + voiture + ", dateDebut=" + dateDebut + ", dateFin=" + dateFin + ", prixLocation=" + prixLocation + ", avecChauffeur=" + avecChauffeur + ", approuved=" + approuved + '}';
    }
    
}
