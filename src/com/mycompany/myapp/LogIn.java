package com.mycompany.myapp;

import com.codename1.capture.Capture;
import com.codename1.components.InfiniteProgress;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.Command;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.Resources;
import com.mycompany.myapp.entity.User;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;


public class LogIn {
    
    
    Form f ;
    TextField login;
    TextField pass;
    Button log;
    Label lblogin;
    Label lbpass;
    private Resources theme;
    
    public LogIn(Resources theme){
        this.theme = theme;
        
    }

    public TextField getlogin() {
        return login;
    }

 
    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }
    
    
    
 public void start(){
     login = new TextField();
        pass = new TextField();
        log = new Button("S'enregister");
        f = new Form("S'enregistrer", BoxLayout.y());
        lblogin= new Label("Log In");
        lbpass= new Label("Mot de passe");
        f.add(lblogin);
        f.add(login);
        f.add(lbpass);
        f.add(pass);
        f.add(log);
        
        Button btnCapture = new Button("Capture");
        btnCapture.addActionListener(new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent evt) {
             Capture.capturePhoto(Display.getInstance().getDisplayWidth(), -1);
         }
        });
//        f.add(btnCapture);
        
//        Command cmd = new Command("go"){
//         @Override
//         public void actionPerformed(ActionEvent evt) {
//             Home home = new Home();
//             home.start();
//         }
//        };
//        f.getToolbar().addCommandToLeftBar(cmd);
//        
        
        
     
     
     
    
        log.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
//              Home home = new Home(theme, login.getText());
//              home.getF().show();
                ConnectionRequest con = new ConnectionRequest(){
                    Map response;
                    java.util.ArrayList content;
                    
                    @Override
                    protected void postResponse() {
                        
                        String status = response.get("status").toString();
                        if(status.equals("true")){
                            System.out.println("ooooooook");
                            content = (java.util.ArrayList) response.get("content");
                            System.out.println(content.get(0).toString());
                             LinkedHashMap user = (LinkedHashMap) content.get(0);
                             User u = new User();
                              Double idDouble = (Double) user.get("id");
                             u.setId(idDouble.intValue());
                             u.setNom((String) user.get("nom"));
                             u.setPrenom((String) user.get("prenom"));
                             u.setRole((String) user.get("role"));
                            Session.setLoggedUser(u);
                            
                            if(u.getRole().equals("ROLE_MANAGER_LOCATION")){
//                                ListeVoitureLocationManager home = new ListeVoitureLocationManager();
//                                home.start();
                               
                                    Home home = new Home();
                                    home.start();
                            }else{
                                HomeClient home = new HomeClient();
                                home.start();
                            }
                           
//                            MyApplication app = new MyApplication();
//                            app.start();
                        }else{
                            System.out.println("noooooooo ");
                            Dialog.show("Erreur", "username ou mot de passe incorrecte", "OK", null);
                        }
                    }
                    
                    

                    @Override
                    protected void readResponse(InputStream input) throws IOException {
                        JSONParser parser = new JSONParser();
                        response = parser.parseJSON(new InputStreamReader(input));
                        System.out.println("res : " + response.toString());
                    }
                    
                    
                    
                };
                con.setPost(false);
                con.setUrl("http://localhost/karhabty_web/Karhabty-WEB/web/app_dev.php/api/login?username=" + login.getText() + "&password=" + pass.getText());
//                InfiniteProgress progress = new InfiniteProgress();
//                Dialog dlg = progress.showInifiniteBlocking();
//                con.setDisposeOnCompletion(dlg);
                
               
                NetworkManager.getInstance().addToQueue(con);
                
                
            }
        });
      f.show(); 
 }
    
}
