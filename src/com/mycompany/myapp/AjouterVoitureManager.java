/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.components.InfiniteProgress;
import com.codename1.components.OnOffSwitch;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.CheckBox;
import com.codename1.ui.Command;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.list.DefaultListModel;
import com.codename1.ui.list.ListModel;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.spinner.GenericSpinner;
import com.codename1.ui.spinner.NumericSpinner;
import com.codename1.ui.spinner.Picker;
import com.codename1.ui.util.Resources;
import com.codename1.ui.util.UIBuilder;
import com.codename1.ui.util.Resources;
import com.mycompany.myapp.entity.Voiture;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Vector;

/**
 *
 * @author 3D-Artist
 */
public class AjouterVoitureManager {
    private Resources theme;
    private Form current;
    Button btn;
    TextField matricule;
    GenericSpinner marque;
    TextField couleur;
    GenericSpinner carburant;
    NumericSpinner kilometrage;
    NumericSpinner puissance;
    GenericSpinner nbreporte;
    TextField prixlocation;
    TextField description;
    Picker date;
    GenericSpinner boite;
    CheckBox gps;
    CheckBox climatisation;
    CheckBox airbag;
    CheckBox alarme;
    CheckBox freinabs;
    Container cnt;
    ConnectionRequest con;
    StringBuffer str;
    int ch;
 
            
    
    public AjouterVoitureManager(){
          
        
    }
    public void start(){
        if(current != null){
            current.show();
            return;
        }
        
        
       

         
          Form f= new Form("Ajouter Voiture", BoxLayout.y());
          
          Command back = new Command("Back"){
                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        System.out.println("Going back now");
                        Home home = new Home();
                        home.start();
                    }
                    
                };
                
                f.getToolbar().setBackCommand(back);
          UIBuilder ui = new UIBuilder();
          //f= ui.createContainer(theme, "Ajouter Voiture").getComponentForm();
     
//          f.getToolbar().addCommandToOverflowMenu("Logout", null, new ActionListener() {
//
//         @Override
//         public void actionPerformed(ActionEvent evt) {
//        LogIn login =  new LogIn(theme);
//        login.getF().showBack();
//         }
//     });
         btn = new Button("Ajouter Voiture");
        //les text field
        TextField matricule=new TextField("","matricule");;
      //
        GenericSpinner marque= new GenericSpinner();
       
        Vector<String> items0 = new Vector<>();
        items0.add("BMW");
        items0.add("PEUGEOT");
        items0.add("FIAT");
        items0.add("FORD");
        items0.add("MINICOOPER");
        items0.add("WOLSVAGEN");
        items0.add("GOLF");
        items0.add("CITROEN");
        items0.add("JEEP WRANGLER");
        items0.add("PEUGEOT");
        ListModel<String> model0 = new DefaultListModel<>(items0);
        marque.setModel(model0);
        TextField couleur=new TextField("","Couleur");
        //TextField carburant=new TextField("","Carburant");
        GenericSpinner carburant= new GenericSpinner();
        Vector<String> items4 = new Vector<>();
        items4.add("Essence");
        items4.add("Diesel");
        ListModel<String> model4 = new DefaultListModel<>(items4);
        carburant.setModel(model4);
        //date mise en circulation
        Picker date=new Picker();
        
        
        
        //TextField kilometrage=new TextField("","Kilometrage");
        //TextField puissance=new TextField("","Puissance");
        
        
        NumericSpinner kilometrage=new NumericSpinner();
        NumericSpinner puissance=new NumericSpinner();
        
        GenericSpinner carrousserie= new GenericSpinner();
        Vector<String> items = new Vector<>();
        items.add("SUV");
        items.add("CITADINE");
        items.add("Coupe");
        items.add("Classique");
        ListModel<String> model = new DefaultListModel<>(items);
        carrousserie.setModel(model);
        //les booleen
       // CheckBox boite=new CheckBox("Boite");
       GenericSpinner boite= new GenericSpinner();
        Vector<String> valeur = new Vector<>();
        valeur.add("manuelle");
        valeur.add("automatique");
        ListModel<String> model1 = new DefaultListModel<>(valeur);
        boite.setModel(model1);
        
        
        
        CheckBox gps=new CheckBox("Gps");
        CheckBox climatisation=new CheckBox("Climatisation");
        CheckBox airbag=new CheckBox("AirBag");
        CheckBox alarme=new CheckBox("Alarme");
        CheckBox freinabs=new CheckBox("Frein ABS");
        //
        //TextField nbreporte=new TextField("","Nbre de porte");
        
        GenericSpinner nbreporte= new GenericSpinner();
        Vector<Integer> items2 = new Vector<>();
        items2.add(2);
        items2.add(4);
        ListModel<Integer> model2 = new DefaultListModel<>(items2);
        nbreporte.setModel(model2);
        TextField prixlocation=new TextField("","Prix de location");
        TextField description=new TextField("","Description");
        
        Label labelCarrousserie = new Label("Carrousserie");
        Label labelBoite = new Label("Boite");
        Label labelnbreporte = new Label("Nombre de porte");
        Label labelMarque = new Label("Marque");
        Label labelCarburant = new Label("Carburant");
        Label labelDateMise = new Label("Date de mise en circulation");
        Label labelMatricule = new Label("Matricule");
        Label labelDescription = new Label("Description du voiture");
        Label labelCouleur = new Label("Couleur");
        Label labelKilometrage = new Label("Kilometrage");
        Label labelPuissance = new Label("Puissance");
        Label labelAutre = new Label("Autres");
        Label labelPrix = new Label("Prix");

        
        
        f.add(labelMatricule);
        f.add(matricule);
        f.add(labelMarque);
        f.add(marque);
        f.add(labelCouleur);
        f.add(couleur);
        f.add(labelCarburant);
        f.add(carburant);
        f.add(labelDateMise);
        f.add(date);
        f.add(labelKilometrage);
        f.add(kilometrage);
        f.add(labelPuissance);
        f.add(puissance);
        f.add(labelCarrousserie);
        f.add(carrousserie);
        f.add(labelBoite);
        f.add(boite);
        f.add(labelAutre);
        f.add(gps);
        f.add(climatisation);
        f.add(airbag);
        f.add(alarme);
        f.add(freinabs);
        f.add(labelnbreporte);
        f.add(nbreporte);
        f.add(labelPrix);
        f.add(prixlocation);
        f.add(labelDescription);
        f.add(description);
        f.add(btn);
        
        
        
        
        String checkAirbag = null;
        String checkGps = null;
        String checkAlarme = null;
        String checkFrein = null;
        String checkClim = null;
        if(airbag.isSelected()) checkAirbag = "true";
        if(gps.isSelected()) checkGps = "true";
        if(alarme.isSelected()) checkAlarme = "true";
        if(freinabs.isSelected()) checkFrein = "true";
        if(climatisation.isSelected()) checkClim = "true";
       
        
        
         btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                //***************Start of button action performed 
                System.out.println(date.getDate());
                
                ConnectionRequest con = new ConnectionRequest(){
                    LinkedHashMap h;
                    LinkedHashMap content = new LinkedHashMap();
                            
                    @Override
                    protected void postResponse() {
                        System.out.println(h.get("status"));
                        boolean status = Boolean.parseBoolean( h.get("status").toString());
                        if(status){
                            //Afficher dialog success
                            content = (LinkedHashMap) h.get("content");
                            for (Object object : content.entrySet()) {
                                Map.Entry me = (Map.Entry) object;
                                System.out.println(me.getValue());
                            }
                            
                            Dialog.show("Succée", "Voiture ajouté", "OK", null);
                        }else{
                            //Afficher dialog error
                            Dialog.show("Erreur", "Entrer un prix valide", "OK", null);
                        }
                        
                    }
                    
                    
                    @Override
                    protected void readResponse(InputStream input) throws IOException {
                        JSONParser parser = new JSONParser();
                        h = (LinkedHashMap) parser.parseJSON(new InputStreamReader(input));
                        
                        
                    }
                    
                };
                con.setPost(false);
                con.setUrl("http://localhost/karhabty_web/Karhabty-WEB/web/app_dev.php/api/voiture/add?matricule=" + matricule.getText() + "&marque=" + marque.getValue() + "&couleur=" + couleur.getText() + "&carburant=" + carburant.getValue() + "&age=" +date.getDate() + "&kilometrage=" + kilometrage.getValue() + "&puissance=" + puissance.getValue() + "&carrousserie=" + carrousserie.getValue() + "&boite="+boite.getValue() + "&gps=" + gps.isSelected() + "&climatisation=" + climatisation.isSelected() + "&airbag="+airbag.isSelected() + "&nbr_porte=" + nbreporte.getValue() + "&frein_abs="+freinabs.isSelected() + "&alarme=" + alarme.isSelected() + "&description=" + description.getText() + "&prix=" + prixlocation.getText() + "&user=" + Session.getLoggedUser().getId() + "&agence=1");
                InfiniteProgress progress = new InfiniteProgress();
                Dialog dlg = progress.showInifiniteBlocking();
                con.setDisposeOnCompletion(dlg);
                /*con.addResponseListener(new ActionListener<NetworkEvent>() {
                    @Override
                    public void actionPerformed(NetworkEvent evt) {
                        System.out.println(con.getResponseData().toString());
                    }
                   
                    
                    
                });*/
               
                NetworkManager.getInstance().addToQueue(con);
                
                
               
        
        
              //********************End of button action performed 
            }
         });
         
         
         
         
         f.show();
         
         
    }
}

                 
            
       
                 

           

            
        
    
        
        
    
      





