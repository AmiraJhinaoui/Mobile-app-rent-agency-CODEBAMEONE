/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.ui.Container;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.util.Resources;


public class Home {
    
    private Form current;
    private Resources theme;
    private Image icon;
    Form hi;
    
    public Home(){
        
      
    }

  
   public void start(){
       
       hi = new Form();
        Toolbar tb = hi.getToolbar();
        
        //Image icon = theme.getImage("index.png");
         Container topBar = BorderLayout.east(new Label("")); 
         Container test = BorderLayout.east(new Label("")); 
         hi.add(test);

        Image im = FontImage.createMaterial(FontImage.MATERIAL_LOCAL_TAXI, "TitleCommand",5).toImage();
        Label Title = new Label("KARHABTY", im, "Label");
        hi.getToolbar().setTitleComponent(Title);
        hi.getToolbar().setTitleCentered(true);
       topBar.add(BorderLayout.NORTH, new Label("KARHABTY", "SidemenuTagline"));
        topBar.add(BorderLayout.SOUTH, new Label(Session.getLoggedUser().getNom() + " " + Session.getLoggedUser().getPrenom(), "username"));
        topBar.setUIID("SideCommand");
        tb.addComponentToSideMenu(topBar);
        
        tb.addMaterialCommandToSideMenu("Home", FontImage.MATERIAL_HOME, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                 

            }
        });
        
        
      
        
        tb.addMaterialCommandToSideMenu("Mes Voitures", FontImage.MATERIAL_LOCAL_TAXI, e -> {
            
             ListeVoitureLocationManager lvm=new ListeVoitureLocationManager();
             lvm.start();
        
        });
        tb.addMaterialCommandToSideMenu("Ajouter Voiture", FontImage.MATERIAL_ADD, e -> {
             AjouterVoitureManager ajv=new AjouterVoitureManager();
             ajv.start();
        });
        
//        tb.addMaterialCommandToSideMenu("Mes locations", FontImage.MATERIAL_ATTACH_MONEY, e -> {
//        });
//        tb.addMaterialCommandToSideMenu("Voir Map", FontImage.MATERIAL_MAP, e -> {
//        });
       
        tb.addMaterialCommandToSideMenu("Se Deconnceter", FontImage.MATERIAL_WARNING, e -> {
            Session.setLoggedUser(null);
            LogIn loginInterface = new LogIn(theme);
            loginInterface.start();
        });
        

         hi.show();
       
   }
   
    
   
    
    
    
}
