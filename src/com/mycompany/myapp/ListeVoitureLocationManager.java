/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.components.ImageViewer;
import com.codename1.components.InfiniteProgress;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.Log;
import com.codename1.io.NetworkManager;
import com.codename1.io.Storage;
import com.codename1.io.Util;
import com.codename1.ui.Button;
import com.codename1.ui.util.Resources;
import com.codename1.ui.util.UIBuilder;
import com.codename1.ui.Container;
import com.codename1.ui.Form;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.list.ContainerList;
import com.codename1.ui.Button;
import com.codename1.ui.Command;
import com.codename1.ui.Component;
import com.codename1.ui.Dialog;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.events.DataChangedListener;
import com.codename1.ui.events.SelectionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.list.ListModel;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.EventDispatcher;
import com.mycompany.myapp.entity.Voiture;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.Map;



/**
 *
 * @author 3D-Artist
 */
public class ListeVoitureLocationManager {

    private Resources theme;
    private Form current;
    Button btn;
    Button testList;
    Form f;
//    ImageViewer img;

    public ListeVoitureLocationManager() {
      f = new Form("Mes voitures", BoxLayout.y());
      current = new Form("Mes voitures", BoxLayout.y());
                f.setScrollableY(false);
        
    }

    public void start(){
        if(current != null){
//            current.show();
//            return;
        }
        
        
        
        
        
        
        
        
        
        
        
        //Start Get List Voitures
        ConnectionRequest con = new ConnectionRequest(){
            Map response;
            java.util.ArrayList listVoitures;

            @Override
            protected void postResponse() {
                listVoitures = (java.util.ArrayList)response.get("root");

                System.out.println(listVoitures.toString());
                
                
                
                Container list = new Container(BoxLayout.y());
                list.setScrollableY(true);
                
                Command back = new Command("Back"){
                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        System.out.println("Going back now");
                        Home home = new Home();
                        home.start();
                    }
                    
                };
                
                f.getToolbar().setBackCommand(back);
        
                
                btn = new Button("Afficher la liste");
                
                Font largePlainSystemFont = Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_PLAIN, Font.SIZE_LARGE);
        
                for (int i = 0; i < listVoitures.size(); i++) {
                    System.out.println(listVoitures.get(i).toString());
                    LinkedHashMap v = (LinkedHashMap) listVoitures.get(i);
                    System.out.println(v.get("marque"));
                    
                    
                    Container voitureCaractsContainer = new Container( BoxLayout.x());
                    Label marque = new Label((String) v.get("marque") + " (" + (String) v.get("couleur") + ")");
                    Label couleur = new Label((String) v.get("couleur"));
                    double prixLocation = (double) v.get("prixLocation");
                    Label prix = new Label(String.valueOf(prixLocation) + " DT");
                    Label horizontalSeparator = new Label(" || ");
                    Label horizontalSeparator2 = new Label(" || ");
                    marque.getUnselectedStyle().setFont(largePlainSystemFont);
                    couleur.getUnselectedStyle().setFont(largePlainSystemFont);
                    prix.getUnselectedStyle().setFont(largePlainSystemFont);
                    prix.getAllStyles().setAlignment(Component.RIGHT);
                    
                    
                    
                    
//                    
//                    voitureCaractsContainer.add(marque);
//                    voitureCaractsContainer.add(horizontalSeparator);
//                    voitureCaractsContainer.add(couleur);
//                    voitureCaractsContainer.add(horizontalSeparator2);
//                    voitureCaractsContainer.add(prix);
//                    
//                    list.add(voitureCaractsContainer);
                    
                    
                    
//                    Container box = BoxLayout.encloseX(
//                            marque, 
//                            couleur,
//                            prix);
//                    list.add(box);
                    
                    Container cnt = new Container(new BorderLayout());
                    cnt.add(BorderLayout.WEST, marque);
//                    cnt.add(BorderLayout.NORTH, couleur);
                    cnt.add(BorderLayout.EAST, prix);
                    list.add(cnt);
                    
                    
                    
                    
                    
                    Button btnModifier = new Button("Modifier");
                    
//                    Label marque = new Label((String) v.get("marque"));
//                    Label couleur = new Label((String) v.get("couleur"));
//                    double prixLocation = (double) v.get("prixLocation");
//                    Label prix = new Label(String.valueOf(prixLocation) + " DT");
//
//                    marque.getAllStyles().setAlignment(Component.LEFT);
//                    couleur.getAllStyles().setAlignment(Component.CENTER);
//                    prix.getAllStyles().setAlignment(Component.RIGHT);
//                    
//                      
//                    marque.getUnselectedStyle().setFont(largePlainSystemFont);
//                    couleur.getUnselectedStyle().setFont(largePlainSystemFont);
//                    prix.getUnselectedStyle().setFont(largePlainSystemFont);
//                    
//                    list.add(marque);
//                    list.add(couleur);
//                    list.add(prix);
                    
                    list.add(btnModifier);
                    
                    Label separator = new Label();
                    separator.setText("________________________________________");
                    list.add(separator);
                    
                    Voiture voiture = new Voiture();
                    voiture.setMatricule((String) v.get("matricule"));
                    voiture.setMarque((String) v.get("marque"));
                    voiture.setCouleur((String) v.get("couleur"));
                    voiture.setCarburant((String) v.get("carburant"));
                    //voiture.setAge((String) v.get("age"));
                    Double  valKilometrage = (double) v.get("kilometrage");
                    voiture.setKilometrage(valKilometrage.intValue());
                    Double  valPuissance = (double) v.get("puissance");
                    voiture.setPuissance(valPuissance.intValue());
                    voiture.setCarrousserie((String) v.get("carrousserie"));
                    voiture.setBoite((String) v.get("boite"));
                    voiture.setGps(Boolean.parseBoolean((String) v.get("gps")) );
                    voiture.setClimatisation(Boolean.parseBoolean((String) v.get("climatisation")));
                    voiture.setAlarme(Boolean.parseBoolean((String) v.get("alarme")));
                    voiture.setFrein_abs(Boolean.parseBoolean((String) v.get("freinAbs")));
                    Double  valNbrPorte = (double) v.get("nbrPorte");
                    voiture.setNbr_porte(valNbrPorte.intValue());
                    voiture.setDescription((String) v.get("description"));
                    voiture.setPrixLocation((float) prixLocation);
                    System.out.println("---------" + voiture.getPrixLocation());
                    
                    
                    btnModifier.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent evt) {
                            System.out.println(btnModifier.getText());
                            ModifierVoitureManager modifV = new ModifierVoitureManager();
                            ModifierVoitureManager.voiture = voiture;
                            modifV.start();
                        }
                    });
                }
        
                f.add(list);

                //f.add(btn);

               f.show();
               
               
        
        
        
                

                
                
            }

            @Override
            protected void readResponse(InputStream input) throws IOException {
                JSONParser parser = new JSONParser();
                response = parser.parseJSON(new InputStreamReader(input));
                System.out.println("res : " + response.get("root").toString());
                
                
            }

           
            
            
            
            
            
            
            
        };
        con.setPost(false);
        con.setUrl("http://localhost/karhabty_web/Karhabty-WEB/web/app_dev.php/api/voiture/byManager?id_manager=" + Session.getLoggedUser().getId());
//        InfiniteProgress progress = new InfiniteProgress();
//        Dialog dlg = progress.showInifiniteBlocking();
//        con.setDisposeOnCompletion(dlg);
        NetworkManager.getInstance().addToQueue(con);
        //End Get List Voitures
        
        
        
        
        
        
        
       
  }
    
    
    
}

  
  
        
      

