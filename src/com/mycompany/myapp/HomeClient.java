/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.components.InfiniteProgress;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.CheckBox;
import com.codename1.ui.Command;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.Tabs;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.list.DefaultListModel;
import com.codename1.ui.list.ListModel;
import com.codename1.ui.spinner.GenericSpinner;
import com.codename1.ui.util.Resources;
import com.codename1.ui.util.UIBuilder;
import com.mycompany.myapp.entity.Voiture;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Vector;

/**
 *
 * @author esprit
 */
public class HomeClient {
    
    private Form current;
    private Resources theme;
    private Image icon;
    Form hi;
    
    InfiniteProgress progress = new InfiniteProgress();;
    Dialog dilogProgress = new Dialog("Attendez svp");
    
    public HomeClient(){
        dilogProgress.add(progress);
    }

  
   public void start(){
       if(current != null){
            current.show();
            return;
        }
        
       

        hi = new Form(null,BoxLayout.y());
        hi.setScrollable(true);
        Toolbar tb = hi.getToolbar();
        
        //Image icon = theme.getImage("index.png");
         Container topBar = BorderLayout.east(new Label("")); 
         Container test = BorderLayout.east(new Label("")); 
         hi.add(test);

        Image im = FontImage.createMaterial(FontImage.MATERIAL_LOCAL_TAXI, "TitleCommand",5).toImage();
        Label Title = new Label("KARHABTY", im, "Label");
        hi.getToolbar().setTitleComponent(Title);
        hi.getToolbar().setTitleCentered(true);
        topBar.add(BorderLayout.NORTH, new Label("KARHABTY", "SidemenuTagline"));
        topBar.add(BorderLayout.SOUTH, new Label(Session.getLoggedUser().getNom() + " " + Session.getLoggedUser().getPrenom(), "username"));
        topBar.setUIID("SideCommand");
        tb.addComponentToSideMenu(topBar);
        
        tb.addMaterialCommandToSideMenu("Home", FontImage.MATERIAL_HOME, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                 HomeClient homeCl = new HomeClient();
                 homeCl.start();

            }
        });
        
        
      
        
        tb.addMaterialCommandToSideMenu("Liste Voitures", FontImage.MATERIAL_LOCAL_TAXI, e -> {
            
             ListeVoitureLocationClient lvm = new ListeVoitureLocationClient();
             lvm.start();
        
        });
        
        tb.addMaterialCommandToSideMenu("Voir Map", FontImage.MATERIAL_MAP, e -> {
            GoogleMap map = new GoogleMap();
            map.start();
        });
        
//        tb.addMaterialCommandToSideMenu("Mes locations", FontImage.MATERIAL_ATTACH_MONEY, e -> {
//        });
        
        
        tb.addMaterialCommandToSideMenu("Se Deconnceter", FontImage.MATERIAL_WARNING, e -> {
            Session.setLoggedUser(null);
            LogIn loginInterface = new LogIn(theme);
            loginInterface.start();
        });
        
        
        //*******************Zone de Recherche************///
        Button rechercher = new Button("Rechercher Voituree");
        TextField rech = new TextField();
        rech.setHint("couleur...");
        Label lbrech=new Label("Trouver une voiture a louer");
        GenericSpinner marqueRech= new GenericSpinner();
        Vector<String> items0 = new Vector<>();
        items0.add("BMW");
        items0.add("PEUGEOT");
        items0.add("FIAT");
        items0.add("FORD");
        items0.add("MINICOOPER");
        items0.add("WOLSVAGEN");
        items0.add("GOLF");
        items0.add("CITROEN");
        items0.add("JEEP WRANGLER");
        items0.add("PEUGEOT");
        ListModel<String> model0 = new DefaultListModel<>(items0);
        marqueRech.setModel(model0);
         CheckBox gps=new CheckBox("Gps");
        CheckBox climatisation=new CheckBox("Climatisation");
        CheckBox airbag=new CheckBox("AirBag");
        CheckBox alarme=new CheckBox("Alarme");
        CheckBox freinabs=new CheckBox("Frein ABS");
        
        
        hi.add(lbrech);
        hi.add(rech);
        hi.add(marqueRech);
        hi.add(gps);
        hi.add(climatisation);
        hi.add(airbag);
        hi.add(alarme);
        hi.add(freinabs);
        hi.add(rechercher);
        
        
        rechercher.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent evt) {
               System.out.println("beginnn search");
//                dilogProgress.show();
                ConnectionRequest con = new ConnectionRequest(){
            Map response;
            java.util.ArrayList listVoitures;

            @Override
            protected void postResponse() {
//                dilogProgress.dispose();
                progress.remove();
                listVoitures = (java.util.ArrayList)response.get("root");

                System.out.println(listVoitures.toString());
                
                
                
                Container list = new Container(BoxLayout.y());
                list.setScrollableY(true);
                
                
                
                
                Font largePlainSystemFont = Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_PLAIN, Font.SIZE_LARGE);
        
                if(listVoitures.size() != 0){
                    for (int i = 0; i < listVoitures.size(); i++) {
                    System.out.println(listVoitures.get(i).toString());
                    LinkedHashMap v = (LinkedHashMap) listVoitures.get(i);
                    System.out.println(v.get("marque"));
                    
                    
                    Container voitureCaractsContainer = new Container( BoxLayout.x());
                    Label marque = new Label((String) v.get("marque") + " (" + (String) v.get("couleur") + ")");
                    Label couleur = new Label((String) v.get("couleur"));
                    double prixLocation = (double) v.get("prixLocation");
                    Label prix = new Label(String.valueOf(prixLocation) + " DT");
                    Label horizontalSeparator = new Label(" || ");
                    Label horizontalSeparator2 = new Label(" || ");
                    marque.getUnselectedStyle().setFont(largePlainSystemFont);
                    couleur.getUnselectedStyle().setFont(largePlainSystemFont);
                    prix.getUnselectedStyle().setFont(largePlainSystemFont);
                    prix.getAllStyles().setAlignment(Component.RIGHT);
                    
                    
                    
                    
//                    
//                    voitureCaractsContainer.add(marque);
//                    voitureCaractsContainer.add(horizontalSeparator);
//                    voitureCaractsContainer.add(couleur);
//                    voitureCaractsContainer.add(horizontalSeparator2);
//                    voitureCaractsContainer.add(prix);
//                    
//                    list.add(voitureCaractsContainer);
                    
                    
                    
//                    Container box = BoxLayout.encloseX(
//                            marque, 
//                            couleur,
//                            prix);
//                    list.add(box);
                    
                    Container cnt = new Container(new BorderLayout());
                    cnt.add(BorderLayout.WEST, marque);
//                    cnt.add(BorderLayout.NORTH, couleur);
                    cnt.add(BorderLayout.EAST, prix);
                    list.add(cnt);
                    
                    
                    
                    
                    
                    Button btnModifier = new Button("Reserver");
                    
//                    Label marque = new Label((String) v.get("marque"));
//                    Label couleur = new Label((String) v.get("couleur"));
//                    double prixLocation = (double) v.get("prixLocation");
//                    Label prix = new Label(String.valueOf(prixLocation) + " DT");
//
//                    marque.getAllStyles().setAlignment(Component.LEFT);
//                    couleur.getAllStyles().setAlignment(Component.CENTER);
//                    prix.getAllStyles().setAlignment(Component.RIGHT);
//                    
//                      
//                    marque.getUnselectedStyle().setFont(largePlainSystemFont);
//                    couleur.getUnselectedStyle().setFont(largePlainSystemFont);
//                    prix.getUnselectedStyle().setFont(largePlainSystemFont);
//                    
//                    list.add(marque);
//                    list.add(couleur);
//                    list.add(prix);
                    
                    list.add(btnModifier);
                    
                    Label separator = new Label();
                    separator.setText("________________________________________");
                    list.add(separator);
                    
                    Voiture voiture = new Voiture();
                    voiture.setMatricule((String) v.get("matricule"));
                    voiture.setMarque((String) v.get("marque"));
                    voiture.setCouleur((String) v.get("couleur"));
                    voiture.setCarburant((String) v.get("carburant"));
                    //voiture.setAge((String) v.get("age"));
                    Double  valKilometrage = (double) v.get("kilometrage");
                    voiture.setKilometrage(valKilometrage.intValue());
                    Double  valPuissance = (double) v.get("puissance");
                    voiture.setPuissance(valPuissance.intValue());
                    voiture.setCarrousserie((String) v.get("carrousserie"));
                    voiture.setBoite((String) v.get("boite"));
                    voiture.setGps(Boolean.parseBoolean((String) v.get("gps")) );
                    voiture.setClimatisation(Boolean.parseBoolean((String) v.get("climatisation")));
                    voiture.setAlarme(Boolean.parseBoolean((String) v.get("alarme")));
                    voiture.setFrein_abs(Boolean.parseBoolean((String) v.get("freinAbs")));
                    Double  valNbrPorte = (double) v.get("nbrPorte");
                    voiture.setNbr_porte(valNbrPorte.intValue());
                    voiture.setDescription((String) v.get("description"));
                    voiture.setPrixLocation((float) prixLocation);
                    System.out.println("---------" + voiture.getPrixLocation());
                    
                    
                    btnModifier.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent evt) {
                            System.out.println(btnModifier.getText());
                            ReservationVoiture resV = new ReservationVoiture();
                            ReservationVoiture.voiture = voiture;
                            resV.start();
                        }
                    });
                }
                    
                     hi.setScrollable(false);
                
                hi.removeAll();
                hi.add(list);
                Dialog.show("resultat", listVoitures.size() + " voiture trouvé", "OK", null);
                
                }else{
                    Dialog.show("Pas de resultat", "Aucune voiture trouvé", "OK", null);
                }
                
                
                System.out.println("enddd search");

               
                
//                dilogProgress.dispose();
//                dilogProgress.remove();


              
               
               
        
        
        
                

                
                
            }

            @Override
            protected void readResponse(InputStream input) throws IOException {
                JSONParser parser = new JSONParser();
                response = parser.parseJSON(new InputStreamReader(input));
                System.out.println("res : " + response.get("root").toString());
                
                
            }

           
            
            
            
            
            
            
            
        };
        con.setPost(false);
        con.setUrl("http://localhost/karhabty_web/Karhabty-WEB/web/app_dev.php/api/voiture/search?search_marque=" + marqueRech.getValue() + "&search_input=" + rech.getText() + "&search_gps=" + gps.isSelected() + "&search_climatisation=" + climatisation.isSelected() + "&search_airbag=" + airbag.isSelected() + "&search_frein_abs=" + freinabs.isSelected() + "&search_alarme=" + alarme.isSelected());
//        InfiniteProgress progress = new InfiniteProgress();
//        Dialog dlg = progress.showInifiniteBlocking();
//        con.setDisposeOnCompletion(dlg);
        NetworkManager.getInstance().addToQueue(con);
           }
       });
        
       hi.show();
       
   }
   
    
   
    
    
    
}
