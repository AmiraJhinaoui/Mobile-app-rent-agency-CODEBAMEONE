/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.googlemaps.MapContainer;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import com.codename1.maps.Coord;
import com.codename1.maps.MapListener;
import com.codename1.ui.Button;
import com.codename1.ui.Command;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Font;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.mycompany.myapp.entity.Voiture;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;



public class GoogleMap {
    
    private Form current;
    double lat = 0;
    double lng = 0;
    

    public void init(Object context) {
        try {
            Resources theme = Resources.openLayered("/theme");
            UIManager.getInstance().setThemeProps(theme.getTheme(theme.getThemeResourceNames()[0]));
        } catch(IOException e){
            e.printStackTrace();
        }
    }
    
    public void start() {
        if(current != null){
            current.show();
            return;
        }
        
       
        
        
        
        Form hi = new Form("Voitures");
        
        
        hi.setLayout(new BorderLayout());
//        final MapContainer cnt = new MapContainer("AIzaSyD00UW-yFHi17pDyHLe19_ImRpo0ja5Q3k");
        final MapContainer cnt = new MapContainer();
        hi.addComponent(BorderLayout.CENTER, cnt);
        
        Command back = new Command("Back"){
                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        HomeClient home = new HomeClient();
                        home.start();
                    }
                    
                };
                
                hi.getToolbar().setBackCommand(back);
        
//        cnt.addMapListener(new MapListener() {
//            @Override
//            public void mapPositionUpdated(Component source, int zoom, Coord center) {
//                cnt.clearMapLayers();
//                
//                cnt.setCameraPosition(new Coord(center));
//                try {
//                    cnt.addMarker(EncodedImage.create("/maps-pin.png"), new Coord(center), "Hi marker", "Optional long description", null);
//                } catch (IOException ex) {
//                }
//
//
//            }
//            
//            
//        });

                
                cnt.setCameraPosition(new Coord(36.8731485, 10.2003888));
                
        
//         cnt.addTapListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent evt) {
//                cnt.clearMapLayers();
//                Coord cameraPosition = cnt.getCameraPosition();
//                try {
//                    cnt.addMarker(EncodedImage.create("/maps-pin.png"), new Coord(cameraPosition), "Hi marker", "Optional long description", null);
//                    lat = cnt.getCameraPosition().getLatitude();
//                    lng = cnt.getCameraPosition().getLongitude();
//                } catch (IOException ex) {
//                }
//            }
//        });
       
       
       
        hi.show();
        addMarkers(cnt);
        
    }
    
   
            
    
    public void addMarkers(MapContainer cnt){
//        Coord cameraPosition = new Coord(36.8731485, 10.2003888);
//                try {
//                    
//                    cnt.addMarker(EncodedImage.create("/maps-pin.png"), new Coord(cameraPosition), "Hi marker", "Optional long description", new ActionListener() {
//                        @Override
//                        public void actionPerformed(ActionEvent evt) {
//                            Dialog.show("Agence bla", "the position of agence bla", "OK", null);
//                        }
//                    });
//                    lat = cnt.getCameraPosition().getLatitude();
//                    lng = cnt.getCameraPosition().getLongitude();
//                } catch (IOException ex) {
//                }

            


            ConnectionRequest con = new ConnectionRequest(){
            Map response;
            java.util.ArrayList listVoitures;

            @Override
            protected void postResponse() {
                listVoitures = (java.util.ArrayList)response.get("root");

                 
                for (int i = 0; i < listVoitures.size(); i++) {
                    LinkedHashMap v = (LinkedHashMap) listVoitures.get(i);
                    LinkedHashMap a = (LinkedHashMap) v.get("agence");
                    System.out.println(v.get("agence"));
                    System.out.println("---------------------");
                    System.out.println(a.get("latitude"));
                    System.out.println("*********************");
                    
                    Coord cameraPosition = new Coord((double)a.get("latitude"), (double)a.get("longitude"));
                try {
                    
                    cnt.addMarker(EncodedImage.create("/maps-pin.png"), new Coord(cameraPosition), "Hi marker", "Optional long description", new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent evt) {
                            Dialog.show((String)v.get("marque") + " (" + (double)v.get("prixLocation") + " DT)", "Agence : " + (String)a.get("nomAgence"), "OK", null);
                        }
                    });
                    lat = cnt.getCameraPosition().getLatitude();
                    lng = cnt.getCameraPosition().getLongitude();
                } catch (IOException ex) {
                }
                    
                     
                }
        
               
            }

            @Override
            protected void readResponse(InputStream input) throws IOException {
                JSONParser parser = new JSONParser();
                response = parser.parseJSON(new InputStreamReader(input));
                
                
            }

           
            
            
            
            
            
            
            
        };
        con.setPost(false);
        con.setUrl("http://localhost/karhabty_web/Karhabty-WEB/web/app_dev.php/api/allVoitures");
//        InfiniteProgress progress = new InfiniteProgress();
//        Dialog dlg = progress.showInifiniteBlocking();
//        con.setDisposeOnCompletion(dlg);
        NetworkManager.getInstance().addToQueue(con);
    }

    public void stop() {
        current = Display.getInstance().getCurrent();
    }
    
    public void destroy() {
    }
    
}
