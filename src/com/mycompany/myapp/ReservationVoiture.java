/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.components.InfiniteProgress;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.Command;
import com.codename1.ui.Dialog;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.spinner.Picker;
import com.codename1.ui.util.Resources;
import com.mycompany.myapp.entity.Voiture;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author 3D-Artist
 */
public class ReservationVoiture {
   private Resources theme;
   private Form current; 
   
   public static Voiture voiture = null;
  
  
   



  public ReservationVoiture() {
        
    }

public void start(){
        if(current != null){
            current.show();
            return;
        }


    Form f=new Form("Louer Voiture", BoxLayout.y());

                Command back = new Command("Back"){
                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        System.out.println("Going back now");
                        ListeVoitureLocationClient listeV = new ListeVoitureLocationClient();
                        listeV.start();
                    }
                    
                };
                
                f.getToolbar().setBackCommand(back);

Button louer=new Button("LOuer");
Picker dateDebut=new Picker();
Picker dateFin=new Picker();
Label labelMarque=new Label("Marque");
labelMarque.setText("Marque : " + voiture.getMarque());
Label labelPrix=new Label("Prix");
Label separator = new Label("_______________________________________________________________________________");
labelPrix.setText("Prix : " + voiture.getPrixLocation() + " DT");
Label labeldd=new Label("date de debut");
Label labeddf=new Label("date de fin");

f.add(labelMarque);
f.add(labelPrix);
f.add(separator);
f.add(labeldd);
f.add(dateDebut);
f.add(labeddf);
f.add(dateFin);
f.add(louer);
f.setScrollable(false);


   ///////////********************** boutton louer*******////
    louer.addActionListener(new ActionListener(){
     @Override
             public void actionPerformed(ActionEvent evt) {
               ConnectionRequest con = new ConnectionRequest(){
                    LinkedHashMap h;
                    LinkedHashMap content = new LinkedHashMap();
                            
                    @Override
                    protected void postResponse() {
                        System.out.println(h.get("status"));
                        boolean status = Boolean.parseBoolean( h.get("status").toString());
                        if(status){
                            //Afficher dialog success
                            content = (LinkedHashMap) h.get("content");
                            for (Object object : content.entrySet()) {
                                Map.Entry me = (Map.Entry) object;
                                System.out.println(me.getValue());
                            }
                            
                            Dialog.show("Succée", "Voiture reservé", "OK", null);
                        }else{
                            //Afficher dialog error
                            Dialog.show("Erreur", "Vous avez deja reserver cette voiture", "OK", null);
                        }
                        
                    }
                    
                    
                    @Override
                    protected void readResponse(InputStream input) throws IOException {
                        JSONParser parser = new JSONParser();
                        h = (LinkedHashMap) parser.parseJSON(new InputStreamReader(input));
                        
                        
                    }
                    
                };
                con.setPost(false);
                con.setUrl("http://localhost/karhabty_web/Karhabty-WEB/web/app_dev.php/api/voiture/reserver?matricule=" + voiture.getMatricule() + "&id_user=" + Session.getLoggedUser().getId() + "&date_debut=" + dateDebut.getDate() + "&date_fin=" + dateFin.getDate());
                InfiniteProgress progress = new InfiniteProgress();
                Dialog dlg = progress.showInifiniteBlocking();
                con.setDisposeOnCompletion(dlg);
                /*con.addResponseListener(new ActionListener<NetworkEvent>() {
                    @Override
                    public void actionPerformed(NetworkEvent evt) {
                        System.out.println(con.getResponseData().toString());
                    }
                   
                    
                    
                });*/
               
                NetworkManager.getInstance().addToQueue(con);


             }


              });
              

f.show();

}






}
